(function($){
  $(document).ready(function(){
	deals_city = $.cookie('deals_city');
	if(deals_city!='' && deals_city!=null){
	  $('.view-cities-list select option').removeAttr('selected');
      $('.view-cities-list select option[value$="'+deals_city+'"]').attr('selected','selected');
	}

	$('.view-cities-list select').unbind().bind('change', function(){
	  $.cookie('deals_city', this.value, { expires: 30, path: '/'});
	  window.location.reload();
	  return false;
	});
  });
})(jQuery);